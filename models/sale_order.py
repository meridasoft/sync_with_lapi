# -*- coding: utf-8 -*-
from odoo import models
from zeep import Client


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        cliente = Client('https://lapiweb.com.mx/WSSAmarilla_Prueba/ws_samarilla.php?wsdl')
        data = {
            "NoSucursal": "123",
            "NoSubSucursal": "123",
            "RefeOrden": "123",
            "CveEmpresa": "123",
            "Nombre": "123",
            "Paterno": "123",
            "Materno": "123",
            "Genero": "123",
            "FNacimiento": "123",
            "CorreoPaciente": "123",
            "Estudio1": "123",
            "Estudio2": "123",
            "Estudio3": "123",
            "Estudio4": "123",
            "Estudio5": "123",
            "Estudio6": "123",
            "Estudio7": "123",
            "Estudio9": "123",
            "Estudio10": "123",
            "observa": "123",
        }
        respuesta = cliente.service.AltadePreOrden(
            NoSucursal="1233",
            NoSubSucursal="123",
            RefeOrden="123",
            CveEmpresa="123",
            Nombre="123",
            Paterno="123",
            Materno="123",
            Genero="123",
            FNacimiento="123",
            CorreoPaciente="123",
            Estudio1="123",
            Estudio2="123",
            Estudio3="123",
            Estudio4="123",
            Estudio5="123",
            Estudio6="123",
            Estudio7="123",
            Estudio9="123",
            Estudio10="123",
            observa="123"
        )
        print("hola")
        return res
