# -*- coding: utf-8 -*-

{
    'name': 'sync with lapi',
    'version': '15.0',
    'description': 'Sincroniza con api de LAPI y genera citas conforme a los pedidos de venta',
    'author': 'Meridasoft',
    'license': 'LGPL-3',
    'category': '',
    'depends': [
        'sale_management',
    ],
    'data': [
    ],
    # 'images': ['static/src/img/icon.png'],
    # only loaded in demonstration mode
    'demo': [
    ],
    'auto_install': False,
    'application': False,
    'installable': True,
}
